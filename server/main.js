import { Meteor } from 'meteor/meteor';

Players = new Mongo.Collection("players");
Scenes = new Mongo.Collection("scenes");
Videos = new Mongo.Collection("videos");
Params = new Mongo.Collection("params");

var durtot = 55*60;

var players = [
  {name:"OLIVIER",score:1000},
  {name:"EDOUARD",score:720},
  {name:"NICOLAS",score:940}];

var scenes = [
  {name:''}, // nom vide
  {name:'Hot potatoe'},     // 1
  {name:'In the shit'},     // 2
  {name:'Again in the shit'}, // 3
  {name:'Again in the shit again'}, // 4
  {name:'Again in the shit again and again'}, // 5
  {name:'Again and again in the shit again and again'}, // 6
  {name:'We are all gonne die'}, // 7
  {name:'Super Heros'}, //8
  {name:'What Where Word'}, //9
  {name:'Wolf Garou'}, //10
  {name:'Old job / New Job'}, //11
  {name:'Guilty'}, //12
  {name:'Wolf Garou'}, //13
  {name:'Security check'}, //14
  {name:'Teleshopping'}, //15
  {name:'All the truth in my head'}, //16
  {name:'Thanks god you are here'}, //17
  {name:'Two lines vocabulary'}, //18
  {name:'If you know what I mean'}, //19
  {name:'Psychopath'}, //20
  {name:'Who\'s the motherfucker'}, //21
  {name:'What\’s in the bag'}, //22
  {name:'The little voice'}, //23
  {name:'News flash'}, //24
  {name:'Bachelor'}, //25
  {name:'Threesong'}, //26
  {name:'If you know what I mean'}, //27
  {name:'Can you feel it'}, //28
  {name:'Picnic'}, //29
  {name:'You are not alone'}, //30
  {name:'Touch me I\'m famous'}, //31
  {name:'The Shit! Always in the shit'}, //32
  {name:'What the fuck with this shit ?'}, //33
  {name:'Hot fires'}, //34
  {name:'He says / She says'}, //35
  {name:'Good Cop Bap Cop'}, //36
  {name:'CLASHIFICATION'} //40
];

var liste = [
  -1, // nothing
  0,   // generique debut

  2,
  98, // applaudimetre
  97, // roue

  18,
  98, // applaudimetre
  97, // roue

  3,
  98, // applaudimetre
  97, // roue

  36,
  98, // applaudimetre
  97, // roue

  4,
  98, // applaudimetre
  97, // roue

  34,
  98, // applaudimetre
  97, // roue

  11,
  98, // applaudimetre
  97, // roue

  5,
  98, // applaudimetre
  97, // roue

  35,
  98, // applaudimetre
  97, // roue

  6,
  98, // applaudimetre
  97, // roue

  27,
  98, // applaudimetre
  97, // roue

  12,
  98, // applaudimetre
  97, // roue

  32,
  98, // applaudimetre
  97, // roue

  33,
  98, // applaudimetre
  97, // roue
  99, // CLASHIFICATION
  100, // générique fin
];

const cartes = [
  '-10',
  '-100',
  'Mutltiplie ton score par 2 ',
  'Remets un de tes collègues à 0',
  'Echange ton score avec le dernier',
  'Prends un score supplémentaire entre 10 et 100',
  'Prends la moitié des points de chacun des deux autres'
]

var videos = [
  {name:'Début',file:'videos/01-Intro Clash.MP4'},
  {name:'CLASH No',file:'videos/02-Annonce categorie (bruitage).MP4'},
  {name:'Applaudimètre',file:'videos/03-Applaudimetre.MP4'},
  {name:'Sirène',file:'videos/04-Fin du temps.MP4'},
  {name:'fin',file:'videos/05-finclash.mp4'},
  //{name:'Générique Fin',file:'videos/03-Fin de clash.MP4'},
  // {name:'roue -10',file:'videos/Roue -10.MP4'},
  // {name:'roue -100',file:'videos/Roue -100.MP4'},
  {name:'roue 20',file:'videos/Roue 20.MP4'},
  {name:'roue 30',file:'videos/Roue 30.MP4'},
  {name:'roue 40',file:'videos/Roue 40.MP4'},
  {name:'roue 50',file:'videos/Roue 50.MP4'},
  {name:'roue 60',file:'videos/Roue 60.MP4'},
  {name:'roue 70',file:'videos/Roue 70.MP4'},
  {name:'roue 80',file:'videos/Roue 80.MP4'},
  {name:'roue 90',file:'videos/Roue 90.MP4'},
  {name:'roue 100',file:'videos/Roue 100.MP4'},
  {name:'roue 200',file:'videos/Roue 200.MP4'},
  {name:'fuck1',file:'videos/Roue fuck1.MP4'},
  {name:'fuck2',file:'videos/Roue fuck2.MP4'},
  {name:'fuck3',file:'videos/Roue fuck3.MP4'},
  {name:'fuck4',file:'videos/Roue fuck4.MP4'},
];

var params = [
  {param:'currentscene',val:''},
  {param:'nclash',val:0},
  {param:'selectedplayer',val:0},
  {param:'carte',val:''},
  {param:'timer',val:durtot}
]

function playVideo(vid) {
  Params.update({param:'carte'},{$set:{val:''}});
  // hide players
  Players.update( {}, {$set: {hidden:true}} , {multi:true}  )
  // stop current video
  Videos.update({}, {$set:{playing:false}}, {multi:true})
  // play vid
  Videos.find().forEach( function(v) {
    Videos.update( v, {$set: {playing: (v.id==vid) } } )
  })
}


var curid=undefined;

function starttimer() {

  Params.update( {param:'timer'}, {$set: {val:durtot}});
  var curdate = new Date();

  if (curid!=undefined) return;

  curid = Meteor.setInterval( function() {

    var now = new Date();
    var sec = durtot - Math.floor( (now - curdate)/1000 );
    Params.update( {param:'timer'}, {$set: {val:sec}});

    if (sec<=0) {
      Meteor.clearInterval(curid);
      Params.update( {param:'timer'}, {$set: {val:0}});
      playVideo(4);
    }
  },1000);
}

var nstep = 0;

function step(d) {
  Params.update({param:'carte'},{$set:{val:''}});

  nstep+=d;

  if (nstep<=0) nstep=0;
  // var p = Params.findOne({param:'nclash'})
  // var nc = parseInt(p.val);
  var l = liste[nstep];
  var nc=0;

  switch(l) {
    case -1:
      break;
    case 0:
      if (d==-1) nc=-1;
      playVideo(1);
      break;
    case 97: // ROUE
      var r = Math.floor(6 + Math.random()*13);
      playVideo(r);
      if (r>15)
      var id =  Meteor.setTimeout( function()
        {
        Streamy.broadcast('randomfuck','');
        Meteor.clearInterval(id);
      },8000);
      break;
    case 98: // APPLAUDIMETRE
      if (d==-1) nc=-1;
      playVideo(3);
      Meteor.setTimeout( function() {
        Players.update( {id:1}, {$set: {hidden:false,playing: true}}  );
        Streamy.broadcast('playvideo', { id: 1 }); },
        3000);
      Meteor.setTimeout( function() {
        Players.update( {id:1}, {$set: {hidden:true,playing: false}}  );
        Players.update( {id:2}, {$set: {hidden:false,playing: true}}  );
        Streamy.broadcast('playvideo', { id: 2 }); },
        8000);
      Meteor.setTimeout( function() {
        Players.update( {id:2}, {$set: {hidden:true,playing: false}}  );
        Players.update( {id:3}, {$set: {hidden:false,playing: true}}  );
        Streamy.broadcast('playvideo', { id: 3 }); },
        13000);
      Meteor.setTimeout( function() {
        Players.update( {}, {$set: {hidden:true,playing: false}} , {multi:true}  ); },
        18000);
      break;
    case 99:  // CLASHIFICATION
      playVideo(4);
      break;
    case 100: // FIN
      playVideo(5);
      break;
    default:
      playVideo(2);
      var sc = scenes[l];
      Params.update( {param:'currentscene'}, {$set: {val:sc.name}})
      if (d==1) nc=1;
      if (nstep==2) {
        //Params.update( {param:'timer'}, {$set: {val:durtot}});
        starttimer();
      }
      break;
  }

Params.update( {param:'nclash'}, {$inc:{val:nc}});

}

Meteor.methods( {
  changescene: function (cid) {
    var cs = Scenes.findOne( {id:cid})
    if (cs==undefined) return
    //Scenes.update( {id:0}, {$set: {name: cs.name}} )
    Params.update( {param:'currentscene'}, {$set: {val:cs.name}})
  },
  prev: function() {
    step(-1);
  },
  next: function() {
    step(+1);
  },
  changevideo: function (vid) {
    // if (vid==2) {
    //       Params.update( {param:'nclash'}, {$inc:{val:1}});
    // }
    Players.update( {}, {$set: {hidden:true}} , {multi:true}  )
    Videos.find().forEach( function(v) {
      Videos.update( v, {$set: {playing: (v.id==vid) } } )
    })
  },
  selectplayer: function(id) {
    Players.update( {}, {$set: {hidden:false}} , {multi:true}  )
    var p = Players.findOne( {id:id} )
    Players.update( {}, {$set: {playing: false}}, {multi: true})
    if (p.playing!=true)
    {
      Players.update( {id:id}, {$set: {playing:true}} )
      Streamy.broadcast('playvideo', { id: id });
    }
  },
  playvideo: function() {
  },
  pausevideo: function() {
    Videos.update({}, {$set:{playing:false}}, {multi:true})
  },
  addscore: function (id,s) {
    Players.update( {id:id}, {$inc: {score: s}} )
  },
  hide: function() {
    Players.update( {}, {$set: {hidden:true}} , {multi:true}  )
  },
  show: function() {
    Params.update({param:'carte'},{$set:{val:''}});
    Players.update( {}, {$set: {playing: false}}, {multi: true})
    Players.update( {}, {$set: {hidden:false}} , {multi:true}  )
  },
  reset: function() {
    Players.update( {}, {$set: {score:0}} , {multi:true}  )
  },
  random: function() {
    var t = cartes[Math.floor((Math.random()*cartes.length))];
    Params.update({param:'carte'},{$set:{val:t}});
  },
  nocarte: function() {
    Params.update({param:'carte'},{$set:{val:''}});
  }
})


Meteor.startup(() => {

    Players.remove({});
    Scenes.remove({});
    Videos.remove({});
    Params.remove({});

    //if (Players.find().count() === 0)
    //{
      var id=1
      _.each(players, function (p) {
        Players.insert({
          id: id++ ,
          name: p.name,
          playing: false,
          hidden:true,
          score: p.score,
        })
      })
    //}

    //if (Scenes.find().count() === 0) {
      var id=0
      _.each(scenes, function (s) {
        Scenes.insert({
          id: id++ ,
          name: s.name,
        })
      })

      var id=1
      _.each(videos, function (s) {
        Videos.insert({
          id: id++ ,
          name: s.name,
          file: s.file,
          playing: false
        })
      })

      _.each(params, function (p) {
        Params.insert({
          param: p.param,
          val: p.val,
        })
      })
});
