import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { FlowRouter } from 'meteor/kadira:flow-router'
//import { buzz } from 'meteor/brentjanderson:buzz'

Players = new Mongo.Collection("players")
Scenes = new Mongo.Collection("scenes")
Videos = new Mongo.Collection("videos")
Params = new Mongo.Collection("params");

import './main.html';


Streamy.on('playvideo', function(data) {
  $('#avatar'+data.id).trigger('play');
  Meteor.setTimeout(function() {
    $('#avatar'+data.id).trigger('pause');
  }, 5000);
});

Streamy.on('randomfuck', function(data) {
  //console.log("-randomfuck");
  var tot = 5;
  var id = Meteor.setInterval( function() {
    //console.log("---random");
    Meteor.call("random")
    if (tot--<0)
    Meteor.clearInterval(id);
  }, 200)
});

Template.scores.onCreated(function() {
  cursor = Videos.find()
  cursor.observeChanges({
    changed(id,fields) {
        $('#idaudio').trigger("pause");

        var v = Videos.findOne({playing:true})
        if (v==undefined) {
      	  $('#myVideo').trigger('pause')
          //$('#idns').animate({opacity:0},500);
          $('#idsname').animate({opacity:0},500);
      	  return
      	}
        var file = v.file;
        if (v.id==2) { // CLASH No
	         //$('#idns').delay(500).animate({opacity:1},5000);
           var cs = Params.findOne({param:'nclash'})
           if (cs) {
             var nc = parseInt(cs.val)+1;
             //console.log("NCLASH:"+nc)
             $('#idaudio').trigger("pause");
             $('#idaudiosrc').attr("src","/musiques/C"+nc+".mp3")
             $('#idaudio').trigger("load");
             $('#idaudio').trigger("play");
             Meteor.setTimeout(function() {
               $('#idaudio').trigger("pause");
               $('#idaudiosrc').attr("src","/musiques/jingle.mp3")
               $('#idaudio').trigger("load");
               $('#idaudio').trigger("play");
             },3000);
           }


           $('#idsname').delay(800).animate({opacity:1},6000);
           $('#idtimer').animate({opacity:1},500);
        }
        if (v.id==3) { // applaudimetre
          // var s = new buzz.sound('/musiques/1.mp3');
          // s.play();
          var r = Math.floor(1 + Math.random()*45);
          $('#idaudio').trigger("pause");
          $('#idaudiosrc').attr("src","/musiques/"+r+".mp3")
          $('#idaudio').trigger("load");
          $('#idaudio').trigger("play");
        }
        $('#myVideo').trigger('pause')
      	$('#myVideo source').attr('src', file);
       	$('#myVideo').trigger('load')
        $('#myVideo').trigger('play')
        Meteor.setTimeout(function() {
          //$('#idns').animate({opacity:0},500);
          $('#idsname').animate({opacity:0},500);
        },500);
    }
  })
})


Template.scores.helpers({
    players: function () {
      // return Players.find({}, { sort: { score: 1,id:1} })
      return Players.find({}, { sort: { id:1} })
    },
    scenes: function () {
      return Scenes.find({})
    },
    ns: function() {
      //var cs = Scenes.findOne({id:0})
      var cs = Params.findOne({param:'nclash'})
      if (cs==undefined) return "";
      return parseInt(cs.val);
    },
    timer: function() {
      var t = Params.findOne({param:'timer'})
      if (t==undefined) return "";
      var sec = ("0" + Math.floor( (t.val) % 60 )).slice(-2);
      var min = ("0" + Math.floor( (t.val/60) % 60 )).slice(-2);

      return min+":"+sec;
    },
    scene: function () {
      //return Scenes.findOne({id:0})
      return Params.findOne({param:'currentscene'})
    },
    carte: function() {
      return Params.findOne({param:'carte'})
    }
  })

  Template.admin.helpers({
    players: function () {
      return Players.find({});
    },
    scenes: function () {
      return Scenes.find({},{ sort: { name: 1} })
    },
    videos: function () {
      return Videos.find({})
    },
    scene: function () {
      //return Scenes.findOne({id:0})
      return Params.findOne({param:'currentscene'})
    },
    notnull: function (x) { return (x!=0) },
  })

  Template.admin.events({
    'click .prev': function() {
      Meteor.call('prev')
    },
    'click .next': function() {
      Meteor.call('next')
    },
    'click #idsp': function(event,template) {
      var idp = parseInt($(event.target).attr('idp'));
      Meteor.call('selectplayer', idp);
    },
    "click #idrandom": function(event,template){
      var tot = 20;
      var id = Meteor.setInterval( function() {
        Meteor.call("random")
        if (tot--<0)
        Meteor.clearInterval(id);
      }, 100)
    },
    "click #idnocarte": function(event,template){
      Meteor.call("nocarte")
    },
    "click #idsuspens": function() {
      $('#idaudio').trigger("pause");
      $('#idaudiosrc').attr("src","/musiques/suspens.mp3")
      $('#idaudio').trigger("load");
      $('#idaudio').trigger("play");
    },
    "click #idlove": function() {
      $('#idaudio').trigger("pause");
      $('#idaudiosrc').attr("src","/musiques/love.mp3")
      $('#idaudio').trigger("load");
      $('#idaudio').trigger("play");
    },
    "click #idplay": function() {
      $('#idaudio').trigger("play");
    },
    "click #idstop": function() {
      $('#idaudio').trigger("pause");
      Meteor.call('pausevideo');
    }
  });

  Template.player.helpers({
    selected: function () {
      return (this.playing)?'selected':'';
    },
    hidden: function () {
      return (this.hidden)?'hidden':'shown';
    }

  });


FlowRouter.route('/', {
  name: 'scenes',
  action: function(params, queryParams) {
    BlazeLayout.render('mondial', {main: 'scores'});
  }
});

FlowRouter.route('/admin', {
  name: 'admin',
  action: function(params, queryParams) {
  FlowRouter.go("/admin/1")
}
});

FlowRouter.route('/admin/:_id', {
  name: 'admin',
  action: function(params, queryParams) {
    //currentid = parseInt(params._id)
    //Meteor.call('changescene', currentid )
  BlazeLayout.render('mondial', {main: 'admin'});
}
});
